import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from ui.pages.login_page import LoginPage


class TestLogin:

    _driver = None

    def setup_method(self):
        self._driver = webdriver.Chrome()

    def teardown_method(self):
        self._driver.quit()

    def test_login(self):
        page = LoginPage(self._driver)
        page.load()

        page.username_text_field = 'shumakov.e@gmail.com'
        page.password_text_field = 'wA7qJofssvx-TTr_'
        page.sign_in_button.click()
        time.sleep(4)
        assert self._driver.current_url == 'https://phone.aircall.io/onboarding', "Login test failed"

    def test_invalid_login(self):
        page = LoginPage(self._driver)
        page.load()
        page.username_text_field = 'fake_user@gmail.com'
        page.password_text_field = 'fakepassword'
        page.sign_in_button.click()
        time.sleep(4)
        assert self._driver.current_url == 'https://phone.aircall.io/login', "Login with invalid credentials succesful"