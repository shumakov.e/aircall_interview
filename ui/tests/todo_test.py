import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from ui.pages.login_page import LoginPage
from ui.pages.todo_page import TodoPage
from ui.pages.onboarding_page import OnboardingPage


def test_todo_archive_button():
    driver = webdriver.Chrome()
    login_page = LoginPage(driver)
    login_page.load()
    login_page.username_text_field = 'shumakov.e@gmail.com'
    login_page.password_text_field = 'wA7qJofssvx-TTr_'
    login_page.sign_in_button.click()

    onboarding_page = OnboardingPage(driver)
    time.sleep(1)
    onboarding_page.next_button.click()
    onboarding_page.next_tip_button.click()
    onboarding_page.next_tip_button.click()
    onboarding_page.next_tip_button.click()
    onboarding_page.discover_button.click()
    todo_page = TodoPage(driver)
    todo_page.load()
    assert todo_page.archive_all_button.is_visible()


