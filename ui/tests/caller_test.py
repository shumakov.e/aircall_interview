import time

import pytest
from selenium import webdriver
from ui.pages.login_page import LoginPage
from ui.pages.caller_page import CallerPage
from ui.pages.onboarding_page import OnboardingPage
from ui.pages.call_session_page import CallSessionPage
from ui.pages.transition import Transition


class TestCaller:

    _driver = None
    _transition = None

    def setup_method(self):
        self._driver = webdriver.Chrome()
        self._transition = Transition(self._driver)

    def teardown_method(self):
        self._driver.quit()
        self._transition = None

    '''
    Checking that all digits can be pressed and the numbers are displays
    '''
    def test_check_digits_in_caller(self):
        self._transition.login()
        caller_page = CallerPage(self._driver)
        caller_page.load()
        caller_page.digit_zero_button.click()
        caller_page.digit_zero_button.click()
        caller_page.digit_one_button.click()
        caller_page.digit_one_button.click()
        caller_page.digit_two_button.click()
        caller_page.digit_two_button.click()
        caller_page.digit_three_button.click()
        caller_page.digit_three_button.click()
        caller_page.digit_four_button.click()
        caller_page.digit_four_button.click()
        caller_page.digit_five_button.click()
        caller_page.digit_five_button.click()
        caller_page.digit_six_button.click()
        caller_page.digit_six_button.click()
        caller_page.digit_seven_button.click()
        caller_page.digit_seven_button.click()
        caller_page.digit_eight_button.click()
        caller_page.digit_eight_button.click()
        caller_page.digit_nine_button.click()
        caller_page.digit_nine_button.click()
        assert caller_page.number_text_field.get_attribute('value') == '00112233445566778899'

    '''
    Check that the call is started when call button was pressed
    '''
    def test_call_correct_number(self):
        self._transition.login()
        caller_page = CallerPage(self._driver)
        caller_page.load()
        caller_page.digit_zero_button.click()
        caller_page.call_button.click()
        assert self._driver.current_url == 'https://phone.aircall.io/keyboard/ringing'

    '''
    Check that the call session can be finished after start voice
    '''
    def test_drop_call_after_start(self):
        self._transition.login()
        caller_page = CallerPage(self._driver)
        caller_page.load()
        caller_page.digit_zero_button.click()
        caller_page.call_button.click()
        call_session_page = CallSessionPage(self._driver)
        time.sleep(1)
        call_session_page.drop_call_button.click()
        assert self._driver.current_url == 'https://phone.aircall.io/keyboard'
