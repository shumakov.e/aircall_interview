# AirCall demo UI tests

This is just and example of how could test coverage be done. In this example was used POM pattern.And small hard code 
classes. In production there is should be also reporting module, integration with CI, docker for services, etc

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install all required packages.

```bash
pip install -r requirements.txt
```

## Usage

```python
cd aircall_interview/
python -m pytest ui/tests/* --html=ui/reports/report.html --self-contained-html