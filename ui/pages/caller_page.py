from ui.pages.elements import WebElement
from ui.pages.base_page import BasePage


class CallerPage(BasePage):

    def __init__(self, web_driver, url=''):
        if not url:
            self._url = 'https://phone.aircall.io/keyboard'
            super().__init__(web_driver, self._url)

    def load(self):
        super().get(self._url)

    number_text_field = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[2]/div/div[2]/div/input')
    digit_zero_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[11]/span')
    digit_one_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[1]/span[1]')
    digit_two_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[2]/span[1]')
    digit_three_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[3]/span[1]')
    digit_four_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[4]/span[1]')
    digit_five_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[5]/span[1]')
    digit_six_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[6]/span[1]')
    digit_seven_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[7]/span[1]')
    digit_eight_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[8]/span[1]')
    digit_nine_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[9]/span[1]')
    asterisk_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[10]/span')
    hash_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[3]/div/div/div[12]/span')
    call_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[4]/div/button')
    current_phone_number_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[4]/button')
    country_button = WebElement(xpath='//*[@id="app"]/div[4]/div/div/div[2]/div/div[1]/button')




