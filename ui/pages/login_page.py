from ui.pages.elements import WebElement
from ui.pages.base_page import BasePage


class LoginPage(BasePage):

    def __init__(self, web_driver, url=''):
        if not url:
            self._url = 'https://phone.aircall.io/login'
            super().__init__(web_driver, self._url)

    def load(self):
        super().get(self._url)

    username_text_field = WebElement(xpath='//*[@id="app"]/div[4]/div[2]/form/div/div[1]/div[1]/div/div/input')
    password_text_field = WebElement(xpath='//*[@id="app"]/div[4]/div[2]/form/div/div[1]/div[2]/div/div/input')
    sign_in_button = WebElement(xpath='//*[@id="app"]/div[4]/div[2]/form/div/div[2]/button[1]')
    forgot_password_link = WebElement(xpath='//*[@id="app"]/div[4]/div[2]/form/div/div[2]/button[2]')





