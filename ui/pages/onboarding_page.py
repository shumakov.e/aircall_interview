from ui.pages.elements import WebElement
from ui.pages.base_page import BasePage


class OnboardingPage(BasePage):

    def __init__(self, web_driver, url=''):
        if not url:
            self._url = 'https://phone.aircall.io/onboarding'
            super().__init__(web_driver, self._url)

    def load(self):
        super().get(self._url)

    change_button = WebElement(xpath='//*[@id="app"]/div[4]/div[2]/div[2]/div[1]/button')
    next_button = WebElement(xpath='//*[@id="app"]/div[4]/div[2]/div[2]/div[2]/button')
    next_tip_button = WebElement(xpath='//*[@id="app"]/div[4]/div[2]/div[3]/button')
    discover_button = WebElement(xpath='//*[@id="app"]/div[4]/div[2]/div[3]/button')

