from ui.pages.elements import WebElement
from ui.pages.base_page import BasePage


class CallSessionPage(BasePage):

    def __init__(self, web_driver, url=''):
        if not url:
            self._url = 'https://phone.aircall.io/keyboard/ringing'
            super().__init__(web_driver, self._url)

    def load(self):
        super().get(self._url)

    drop_call_button = WebElement(xpath='//*[@id="app"]/div[3]/div/div/div[2]/div/div/div[3]/div/button')
