from ui.pages.login_page import LoginPage
from ui.pages.onboarding_page import OnboardingPage
import time


class Transition:

    _driver = None

    def __init__(self, driver):
        self._driver = driver

    def login(self):
        login_page = LoginPage(self._driver)
        login_page.load()
        login_page.username_text_field = 'shumakov.e@gmail.com'
        login_page.password_text_field = 'wA7qJofssvx-TTr_'
        login_page.sign_in_button.click()

        onboarding_page = OnboardingPage(self._driver)
        time.sleep(1)
        onboarding_page.next_button.click()
        onboarding_page.next_tip_button.click()
        onboarding_page.next_tip_button.click()
        onboarding_page.next_tip_button.click()
        onboarding_page.discover_button.click()