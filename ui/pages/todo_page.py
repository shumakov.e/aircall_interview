from ui.pages.base_page import BasePage
from ui.pages.elements import WebElement


class TodoPage(BasePage):

    def __init__(self, web_driver, url=''):
        if not url:
            self._url = 'https://phone.aircall.io/'
            super().__init__(web_driver, self._url)

    def load(self):
        super().get(self._url)

    archive_all_button = WebElement(xpath='//*[@id="app"]/div[4]/div[1]/div[2]/div/button')
    missed_calls_drop_button = WebElement(xpath='//*[@id="app"]/div[4]/div[2]/div/div/div/div/div/div/div')
