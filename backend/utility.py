import requests
from backend.test_framework.setting import *


def clear_contacts():
    response = requests.get(base_url + '/contacts/search' + test_query, auth=(api_id, api_token))
    found_contacts = response.json()
    print(len(found_contacts))
    if len(found_contacts) > 0:
        for contact in found_contacts.get('contacts'):
            try:
                print(contact)
                requests.delete(base_url + '/contacts/' + str(contact['id']), auth=(api_id, api_token))
            except requests.exceptions.RequestException as e:
                raise SystemExit(e)


def create_test_contact(first_name=None):
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    try:
        requests.post(base_url + '/contacts', auth=(api_id, api_token),
                      data=open('backend/tests/test_data/contact_template.json', 'rb'), headers=headers)
    except requests.exceptions.RequestException as e:
        raise SystemExit(e)


def create_dublicate_contact():
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    try:
        requests.post(base_url + '/contacts', auth=(api_id, api_token),
                      data=open('backend/tests/test_data/dublicate_contact_template.json', 'rb'), headers=headers)
    except requests.exceptions.RequestException as e:
        raise SystemExit(e)