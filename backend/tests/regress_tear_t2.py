import json
from backend.test_framework.base_test import *
from backend.test_framework.setting import *
from backend.utility import *


class TestRegress(BaseTest):

    @pytest.mark.order(1)
    def test_get_single_contact_by_valid_number(self):
        clear_contacts()
        create_test_contact()
        with open('backend/tests/test_data/contact_template.json') as json_file:
            contact_template = json.load(json_file)

            response = requests.get(base_url + '/contacts/search' + test_query, auth=(api_id, api_token))
            assert response.json()['contacts'][0]['first_name'] == contact_template['first_name']
            '''
            Actually we can use some kind of serialization to check all necessaries fields
            '''

    @pytest.mark.order(2)
    def test_get_multiple_contacts_by_valid_number(self):
        clear_contacts()
        create_test_contact()
        create_dublicate_contact()
        response = requests.get(base_url + '/contacts/search' + test_query, auth=(api_id, api_token))
        assert len(response.json()['contacts']) == 1  # Here you can see a bug that API allows to create several
        # contacts with the same number, but does not

    @pytest.mark.order(3)
    def test_get_contact_by_another_number(self):
        clear_contacts()
        create_test_contact()
        response = requests.get(base_url + '/contacts/search?order=asc&order_by=created_at&phone_number=112233',
                                auth=(api_id, api_token))
        assert len(response.json()['contacts']) == 0

    @pytest.mark.order(4)
    def test_get_contact_by_valid_number_without_auth(self):
        clear_contacts()
        create_test_contact()
        response = requests.get(base_url + '/contacts/search' + test_query)
        assert response.status_code == 401